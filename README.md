# Bindings for Polish letters in AutoHotkey

These are bindings for Polish characters in [AutoHotkey](https://autohotkey.com/). 
They will emulate the "Polish programmer" keyboard (which is QWERTY based) on any keyboard.

Install it by adding its content to the default Autohotkey file (`AutoHotkey.ahk`)
or by running it though AutoHotkey as a standalone script.
