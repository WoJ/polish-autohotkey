; bindings for Pollish characters

<^>!+A::Send {U+104}
<^>!+C::Send {U+106}
<^>!+E::Send {U+118}
<^>!+L::Send {U+141}
<^>!+N::Send {U+143}
<^>!+O::Send {U+0D3}
<^>!+S::Send {U+15A}
<^>!+Z::Send {U+179}
<^>!+X::Send {U+17B}
<^>!a::Send {U+105}
<^>!c::Send {U+107}
<^>!e::Send {U+119}
<^>!l::Send {U+142}
<^>!n::Send {U+144}
<^>!o::Send {U+0F3}
<^>!s::Send {U+15B}
<^>!z::Send {U+17C}
<^>!x::Send {U+17A}

; EURO symbol moved to the key 'R'
<^>!r::Send {U+20A0}
